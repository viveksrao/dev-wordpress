<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dev_wordpress');

/** MySQL database username */
define('DB_USER', 'devwpuser');

/** MySQL database password */
define('DB_PASSWORD', 'devwppassword');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<zBey[8u`lTkked6ZsKcLP=|XhZW2dkC+<q|}q?fvV0~Y7URl`>5]pDf=~o9GA-?');
define('SECURE_AUTH_KEY',  '21.F^OqL[H`rUj)kl{O-KG<{TOkrj4rMu)2[E)+EYj#%8fgL13g,b[^g{}}}Ehmd');
define('LOGGED_IN_KEY',    'b7~GGu=+a8W@9rBI*w>a88~-AWc]~=E1U9*/FsM#Nus{t[)%_0#-T-7:=utm=C#$');
define('NONCE_KEY',        '&-_bfv>h5C4bJCE%v#=ex1p9IQ5^+rm(YO2t8G={)6VH,z;J7r>m1iX-hUCI`EE+');
define('AUTH_SALT',        'S2e +q:%EK|r*?vJ!|nvfRM[Ug=--$}:E1ogica[)eW5^#5L=zHTy4U:G}-.Ck~`');
define('SECURE_AUTH_SALT', 'W-q=B5@qB/I/~nJ7lI(1`u$]>8b$9ob|(=?[`DOkEd0U]QN3C|`@]{>w/fTkZ}6<');
define('LOGGED_IN_SALT',   ';sQ-};zNbM{~J]z{]<QQJ7vUG#G6Lo4(0Oz}_Injx(cVf.Y=(eCa|_3,75`F&lIi');
define('NONCE_SALT',       'VlzuAnZ,c[Q&AZ^= A22>V1y,h$V+-FLYOErKH(K`Qt}+Hob|7|;[z[3kUayO+`]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpdev_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
